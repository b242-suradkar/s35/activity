const express = require("express");
const mongoose = require("mongoose")

const app = express();
const port = 3001;

//mongodb connection

mongoose.connect("mongodb+srv://VedantS:VedantVSmongo1@cluster0.cuydfoq.mongodb.net/b242_to_do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

// connection to database
let db = mongoose.connection;

//if connection error occures, output will be in console
db.on("error", console.error.bind(console,"connerction error"));

//if connection is successful
db.once("open",() => console.log("we are connected to cloud database"));

// mongoose schemas
//creates a new task schema
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
});

const userSchema = new mongoose.Schema({
	username : String,
	password : String,
	status : {
		type : String,
		default : "user"
	}
})

//task model 

const Task = mongoose.model("Task",taskSchema);

const User = mongoose.model("User",userSchema);

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.post("/tasks",(req,res) => {
	Task.findOne({name : req.body.name},(err,result) => {
		if(result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		}

		else{
			// create ew task and sae to database
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save((saveErr, savedTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New task created ");
				}
			})
		}
	})
})

app.post("/signup",(req,res)=>{
	User.findOne({username: req.body.username},{password:req.body.password},(err,result) =>{
		if(result != null && result.username === req.body.username){
			return res.send("Duplicate userame");
		}
		else {	
				let newUser = new User({
					username : req.body.username,
					password : req.body.password
				});
				newUser.save((saveErr,savedTask) =>{
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return res.status(201).send("New user created");
					}
				})
			}
	})
})

// get requesst to retrive all the tasks

app.get("/tasks",(req,res) => {
	Task.find({},(err,result) => {
		if(err){
			return console.log(err);
			
		}
		else {
			return res.status(200).json({
				data: result
			})
		}
	})
})

app.get("/signup",(req,res) => {
	User.find({},(err,result) => {
		if(err){
			return console.log(err);
			
		}
		else {
			return res.status(200).json({
				data: result
			})
		}
	})
})
app.listen(port, () => console.log(`Server is running at port ${port}`))